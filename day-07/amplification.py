from opcodes import *

def unique_values(settings):
    dic = {}
    for x in settings:
        if x not in dic:
            dic[x] = 1
        else:
            return False
    return True

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    
    thruster = 0
    best_settings = []
    
    amp_a = IntCodeInterpreter()
    amp_b = IntCodeInterpreter()
    amp_c = IntCodeInterpreter()
    amp_d = IntCodeInterpreter()
    amp_e = IntCodeInterpreter()
    
    for a in range(0, 5):
        for b in range(0, 5):
            for c in range(0, 5):
                for d in range(0, 5):
                    for e in range(0, 5):
                        if unique_values([a,b,c,d,e]):
                            amp_a.init(IntCodeProgram(program_string), False)
                            amp_b.init(IntCodeProgram(program_string), False)
                            amp_c.init(IntCodeProgram(program_string), False)
                            amp_d.init(IntCodeProgram(program_string), False)
                            amp_e.init(IntCodeProgram(program_string), False)
                            ao = amp_a.exec([a, 0])
                            bo = amp_b.exec([b, ao])
                            co = amp_c.exec([c, bo])
                            do = amp_d.exec([d, co])
                            eo = amp_e.exec([e, do])
                            if int(eo) > thruster:
                                print(eo, thruster)
                                thruster = int(eo)
                                best_settings = [a,b,c,d,e]
    print (thruster, best_settings)
    