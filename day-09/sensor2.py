from opcodes import IntCodeInterpreter, IntCodeProgram

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    program = IntCodeProgram(program_string)
    interpreter.init(program, False)
    interpreter.exec([2])
