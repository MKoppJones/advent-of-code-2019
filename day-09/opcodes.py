POSITION = 0
IMMEDIATE = 1
RELATIVE = 2

ADD = 1
MULTIPLY = 2
SAVE = 3
READ = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6
LESS_THAN = 7
EQUALS = 8
ADJUST_RELATIVE_BASE = 9

class IntCodeProgram:
    instructions = []
    def __init__(self, program):
        str_instructions = program.split(',')
        self.instructions = [int(num) for num in str_instructions]

class IntCodeInterpreter:
    program = {}
    instruction_pointer = 0
    input_value = []
    output_value = 0
    halt_at = 99
    operation_modes = []
    parameters = []
    allow_halting = False
    halted = False
    relative_base = 0
    
    def __init__(self):
        self.program = {}
        self.instruction_pointer = 0
        self.input_value = []
        self.halted = False
        self.relative_base = 0
        self.allow_halting = False
        
    def init(self, program, allow_halting):
        self.program = program
        self.instruction_pointer = 0
        self.input_value = []
        self.allow_halting = allow_halting
        self.halted = False
        self.relative_base = 0
            
    def exec(self, input_value):
        self.input_value = input_value
        self.halted = False
        while self.program.instructions[self.instruction_pointer] != self.halt_at and not (self.halted and self.allow_halting):
            current_instruction = str(self.program.instructions[self.instruction_pointer]).zfill(5)
            operation = int(current_instruction[-2:])
            self.operation_modes = [current_instruction[2], current_instruction[1], current_instruction[0]]
            self.run_operation(operation)
        self.halted = self.program.instructions[self.instruction_pointer] == self.halt_at
        return self.output_value

    def run_operation(self, operation):
        if operation == ADD:
            self.add()
        elif operation == MULTIPLY:
            self.multiply()
        elif operation == SAVE:
            self.save()
        elif operation == READ:
            self.read()
        elif operation == JUMP_IF_TRUE:
            self.jump_if_true()
        elif operation == JUMP_IF_FALSE:
            self.jump_if_false()
        elif operation == LESS_THAN:
            self.less_than()
        elif operation == EQUALS:
            self.equals()
        elif operation == ADJUST_RELATIVE_BASE:
            self.adjust_relative_base()
    
    def verify_memory_space(self, pointer):
        if pointer > len(self.program.instructions) - 1:
            count = pointer - len(self.program.instructions)
            for _ in range(count+1):
                self.program.instructions.append(0)
    
    def get_value(self, pointer, index):
        mode = int(self.operation_modes[index])
        current_value = int(self.program.instructions[pointer])
        
        if mode == POSITION:
            self.verify_memory_space(current_value)
            return int(self.program.instructions[current_value])
        elif mode == IMMEDIATE:
            return current_value
        elif int(mode) == RELATIVE:
            current_value = self.program.instructions[pointer] + self.relative_base
            self.verify_memory_space(current_value)
            return self.program.instructions[current_value]
        

    def get_address(self, pointer, index):
        mode = int(self.operation_modes[index])
        self.verify_memory_space(pointer)
        
        if (mode == POSITION):
            return self.program.instructions[pointer]
        elif (mode == RELATIVE):
            return self.program.instructions[pointer] + self.relative_base

    def write(self, location, value):
        self.verify_memory_space(location)
        self.program.instructions[location] = value

    def add(self):
        param1 = self.get_value(self.instruction_pointer + 1, 0)
        param2 = self.get_value(self.instruction_pointer + 2, 1)
        output_location = self.get_address(self.instruction_pointer + 3, 2)
        
        self.write(output_location, param1 + param2)
        self.instruction_pointer += 4
     
    def multiply(self):
        param1 = self.get_value(self.instruction_pointer + 1, 0)
        param2 = self.get_value(self.instruction_pointer + 2, 1)
        output_location = self.get_address(self.instruction_pointer + 3, 2)
        
        self.write(output_location, param1 * param2)
        self.instruction_pointer += 4
    
    def save(self):
        output_location = self.get_address(self.instruction_pointer + 1, 0)
        
        self.write(output_location, self.input_value.pop(0))
        self.instruction_pointer += 2
    
    def read(self):
        self.output_value = self.get_value(self.instruction_pointer + 1, 0)
        
        print(self.output_value)
        self.halted = True
        self.instruction_pointer += 2
    
    def jump_if_true(self):
        param1 = self.get_value(self.instruction_pointer + 1, 0)
        param2 = self.get_value(self.instruction_pointer + 2, 1)
        
        if param1 is not 0:
            self.instruction_pointer = param2
        else:
            self.instruction_pointer += 3
    
    def jump_if_false(self):
        param1 = self.get_value(self.instruction_pointer + 1, 0)
        param2 = self.get_value(self.instruction_pointer + 2, 1)
        
        if param1 is 0:
            self.instruction_pointer = param2
        else:
            self.instruction_pointer += 3
    
    def less_than(self):
        param1 = self.get_value(self.instruction_pointer + 1, 0)
        param2 = self.get_value(self.instruction_pointer + 2, 1)
        output_location = self.get_address(self.instruction_pointer + 3, 2)
        
        if param1 < param2:
            self.write(output_location, 1)
        else:
            self.write(output_location, 0)
        self.instruction_pointer += 4
    
    def equals(self):
        param1 = self.get_value(self.instruction_pointer + 1, 0)
        param2 = self.get_value(self.instruction_pointer + 2, 1)
        output_location = self.get_address(self.instruction_pointer + 3, 2)
        
        if param1 == param2:
            self.write(output_location, 1)
        else:
            self.write(output_location, 0)
        self.instruction_pointer += 4
        
    def adjust_relative_base(self):
        self.relative_base += self.get_value(self.instruction_pointer + 1, 0)
        self.instruction_pointer += 2