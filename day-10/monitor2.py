import math
from collections import defaultdict

# help from https://explog.in/aoc/2019/AoC10.html
if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    asteroids = []
    
    y = 0
    for line in content:
        x = 0
        for point in line:
            if point == '#':
                asteroids.append([x,y])
            x += 1
        y += 1
        
    base = [13, 17]
    pointer = [13, 0]
    destroyed = []    
    asteroids.remove(base)
    
    vectors = defaultdict(lambda: [])
    angles = set()
    
    # Safe version of atan
    def safeatan(n, d):
        result = None
        if d == 0:
            result = math.pi / 2 if n >= 0 else -math.pi / 2
        else:
            result = math.atan(n / d) 
        return result + (math.pi if d < 0 else 0)
    
    for a in asteroids:
        # Determine the direction of BASE -> A
        vec = (a[0] - base[0], a[1] - base[1])
        
        # Determine magnitude of the vector
        mag = math.sqrt(vec[0]**2 + vec[1]**2)
        
        # Determine the angle to the asteroid
        ang = round(vec[0] / mag, 4), round(vec[1] / mag, 4)
        
        # Store the vector in a list based on angle
        vectors[ang].append(vec)
        
        # Store the angle
        angles.add(ang)
    
    angles = list(angles)
    
    # Sort the angles so we read in the correct direction
    angles.sort(key=lambda x: safeatan(x[1], x[0]))
    
    # For each vector list, sort by magnitude
    for angle in vectors:
        vectors[angle].sort(key=lambda x: x[0]**2 + x[1]**2)
        
    index = 1
    stop = False
    while not stop:
        for angle in angles:
            if index == 200 and vectors[angle]:
                v = vectors[angle][0]
                print(v[0] + base[0], v[1] + base[1])
                stop = True
                
            # For each angle list, remove the first item as we rotate.
            if vectors[angle]:
                vectors[angle].pop(0)
                index += 1