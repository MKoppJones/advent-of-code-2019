from math import hypot

def line_point_collision(x1, y1, x2, y2, px, py):
    d1 = hypot(px-x1, py-y1)
    d2 = hypot(px-x2, py-y2)
    line_length = hypot(x2-x1, y2-y1)
    
    if d1 + d2 >= line_length and d1 + d2 <= line_length:
        return True
    return False

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    asteroids = []
    
    y = 0
    for line in content:
        x = 0
        for point in line:
            if point == '#':
                asteroids.append([x,y])
            x += 1
        y += 1
    candidate_base = []
    candidate_see = 0
    for base in asteroids:
        can_see = 0
        for asteroid in asteroids:
            if asteroid == base:
                continue
            collisions = 0
            for point in asteroids:
                if point == base or asteroid == point:
                    continue
                if line_point_collision(base[0], base[1], asteroid[0], asteroid[1], point[0], point[1]):
                    
                    collisions += 1

            if collisions == 0:
                can_see += 1
                print(can_see)
                
        if candidate_see < can_see:
            candidate_base = base
            candidate_see = can_see
    print(candidate_base, candidate_see)