from opcodes import IntCodeInterpreter, IntCodeProgram
from collections import defaultdict
import random

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    program = IntCodeProgram(program_string)
    interpreter.init(program, True, 1)
    
    steps = []
    area = defaultdict(lambda: 1)
    location = (0, 0)
    
    direction_steps = [
        (0, 0),
        (0, 1),     # North
        (0, -1),    # South
        (-1, 0),    # West
        (1, 0)      # East
    ]
    
    direction = 1
    oxygen = (0,0)
    i = 0
    while i < 1000000:
        direction = random.choice(range(4))+1
        while area.get((direction_steps[direction][0] + location[0], direction_steps[direction][1] + location[1])) == 0:
            direction = random.choice(range(4))+1
        output = interpreter.exec([direction])
        print(direction, output)
        steps.append(direction)
        next_step = direction_steps[direction]
        if output[0] == 0:
            point = (next_step[0] + location[0], next_step[1] + location[1])
            area[point] = 0
        elif output[0] == 1:
            location = (next_step[0] + location[0], next_step[1] + location[1])
            area[location] = 1
        elif output[0] == 2:
            location =  (next_step[0] + location[0], next_step[1] + location[1])
            area[location] = 2
            oxygen = location
        i += 1
    print(2 in area.values())