from opcodes import IntCodeInterpreter, IntCodeProgram

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    program = IntCodeProgram(program_string)
    program.instructions[0] = 2
    interpreter.init(program, True, 3)
    
    joystick = 0
    ball, paddle = None, None
    screen = {}
    score = 0
    while not interpreter.halted:
        data = interpreter.exec([joystick])
        
        if len(data) > 0:
            position = (data[0], data[1]) 
            block = data[2]
            if position not in screen:
                screen[position] = block
            else:
                screen[position] = block
                
            if block == 3:
                ball = position
            elif block == 4:
                paddle = position
                
            if position == (-1, 0):
                score = block
                
        if ball is not None and paddle is not None:
            if ball[0] < paddle[0]:
                joystick = 1
            elif ball[0] > paddle[0]:
                joystick = -1
            else:
                joystick = 0
        
    print (score)