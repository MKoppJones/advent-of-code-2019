if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    input_list = list(content[0])
    input_list = [int(num) for num in input_list]
    
    phase = 1
    base_pattern = [0, 1, 0, -1]
    total = 0
    
    def new_pattern(index):
        iteration = index + 1
        new_list = []
        for x in base_pattern:
            for _ in range(iteration):
                new_list.append(x)
        return new_list
    
    while phase < 101:
        new_list = []
        counter = 0
        
        while counter < len(input_list):
            pattern_pointer = 1
            out = 0
            pattern = new_pattern(counter)
            
            for x in input_list:
                out += x * pattern[pattern_pointer]
                pattern_pointer += 1
                
                if pattern_pointer >= len(pattern):
                    pattern_pointer = 0
                    
            new_list.append(int(str(out)[-1]))
            counter += 1

        input_list = new_list
        phase += 1
        
    print(''.join([str(num) for num in input_list[0:8]]))