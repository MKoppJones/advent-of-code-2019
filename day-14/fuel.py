import math
from collections import defaultdict

if __name__ == "__main__":
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    reactions = {}
    for reaction in content:
        reaction_parts = reaction.split(' => ')
        
        input_materials = reaction_parts[0].split(', ')
        input_materials = [(part.split(' ')[1], int(part.split(' ')[0])) for part in input_materials]
        
        output_materials = reaction_parts[1].split(', ')
        output_materials = [(part.split(' ')[1], int(part.split(' ')[0])) for part in output_materials]
        
        output = output_materials[0][0]
        recipe = (output_materials[0][1], input_materials)
        
        reactions[output] = recipe
    
    inventory = defaultdict(lambda: 0, **{'FUEL': 0})
    
    def unmake(item, count, indent):
        if item == 'ORE':
            return count
        
        total = 0
        
        recipe = reactions[item]
        recipe_yield = recipe[0]
        
        count -= inventory[item]
        
        multiplier = math.ceil(count / recipe_yield)
        
        inventory[item] = multiplier * recipe_yield - count
    
        for ingredient in recipe[1]:
            total += unmake(ingredient[0], multiplier * ingredient[1], indent + 1)
        return total
    
    print(unmake('FUEL', 1, 0))