import math

def calculate_fuel_required(mass):
    fuel = math.floor(mass/3)-2
    if (fuel > 0):
        fuel += calculate_fuel_required(fuel)
    else:
        fuel = 0
    return fuel

content = []
with open('input.txt') as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

def sum_fuel_required():
    total = 0
    for fuel in content:
        total += calculate_fuel_required(int(fuel))
    return total

if __name__ == '__main__':
    print(sum_fuel_required())