import collections

class Vector:
    x = 0
    y = 0
    z = 0
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __str__(self):
        return f'Vector({self.x}, {self.y}, {self.z})'
    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

class Moon:
    position = Vector(0, 0, 0)
    velocity = Vector(0, 0, 0)
    def __init__(self, position, velocity):
        self.position = position
        self.velocity = velocity
    def __str__(self):
        return f'Moon(position={self.position}, velocity={self.velocity})'
    def potential_energy(self):
        return abs(self.position.x) + abs(self.position.y) + abs(self.position.z)
    def kinetic_energy(self):
        return abs(self.velocity.x) + abs(self.velocity.y) + abs(self.velocity.z)
    def total_energy(self):
        return self.potential_energy() * self.kinetic_energy()

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    moons = []
    
    for pos in content:
        clean_pos = pos.replace(' ', '')
        clean_pos = clean_pos[1:-1]
        positions = clean_pos.split(',')
        x = int(positions[0].split('=')[1])
        y = int(positions[1].split('=')[1])
        z = int(positions[2].split('=')[1])
        velocity = Vector(0, 0, 0)
        position = Vector(x, y, z)
        moon = Moon(position, velocity)
        moons.append(moon)
        
    for moon in moons:
        print(moon)
    
    def position_compare(value1, value2, vec1, vec2):
        if value1 is not value2:
            if value1 > value2:
                vec2 += 1
                vec1 -= 1
            else:
                vec2 -= 1
                vec1 += 1
        return vec1, vec2
    
    for x in range(0, 1000):
        # Update velocities
        for moon1 in moons:
            velocity1 = moon1.velocity
            for moon2 in moons:
                moons_not_same = moon1 is not moon2
                if moons_not_same:
                    velocity2 = moon2.velocity
                    velocity1.x += -1 if moon1.position.x > moon2.position.x else 0 if moon1.position.x == moon2.position.x else 1
                    velocity1.y += -1 if moon1.position.y > moon2.position.y else 0 if moon1.position.y == moon2.position.y else 1
                    velocity1.z += -1 if moon1.position.z > moon2.position.z else 0 if moon1.position.z == moon2.position.z else 1
                    moon1.veclocity = velocity1
        
        # Update positions
        for moon in moons:
            moon.position = moon.position + moon.velocity
            print(x, moon)
    
    energy = 0
    for moon in moons:
        print(moon)
        energy += moon.total_energy()
    print(energy)