master = {}

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content][0]
    
    index = 0
    size = 25 * 6
    
    while index < len(content):
        layer = content[index:index+size]
        layer_pixels = {}
        for pixel in layer:
            if pixel not in layer_pixels:
                layer_pixels[pixel] = 0
            layer_pixels[pixel] += 1
        if '0' in master:
            if layer_pixels['0'] < master['0']:
                master = layer_pixels
        else:
            master = layer_pixels
        print(master)
        index += size
    
    print(master['1'] * master['2'])