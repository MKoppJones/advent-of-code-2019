import pytest
from container import *

@pytest.mark.parametrize("password, res", [('111111', True), ('223450', False), ('123789', False)])
def test_password(password, res):
    assert password_valid(password) == res